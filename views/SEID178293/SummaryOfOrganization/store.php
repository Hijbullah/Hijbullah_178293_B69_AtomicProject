<?php

require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\SummaryOfOrganization\SummaryOfOrganization;

$obj = new SummaryOfOrganization();

$obj->setData($_POST);


$obj->store();

Utility::redirect('index.php');