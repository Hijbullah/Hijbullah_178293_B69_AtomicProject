<?php
   require_once ("../../../vendor/autoload.php");
   if(!isset($_SESSION)) session_start();
   use App\Message\Message;

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Summary of Organization | Create</title>
    
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    
    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    
</head>
<body>

<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center" >
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>



<div class="container" style="margin-top: 100px">

    <h1 style="text-align: center"> Summary of Organization - Add Form </h1>

    <div class="col-md-2"> </div>


    <div class="col-md-8" style="margin-top: 50px; margin-bottom: 50px">


        <form action="store.php" method="post">

            <div class="form-group">
                <label for="name">Organization Name</label>
                <input type="text" id="name" class="form-control" name="OrganizationName"  placeholder="Please Enter organization Name Here...." required>
            </div>



            <div class="form-group">
                <label for="summary">Summary</label>
                <textarea class="form-control" rows="10" name="Summary" id="summary" placeholder="Write Something about the Organization ....." required></textarea>
            </div>



            <button type="submit" class="btn btn-success" style="margin-right: 10px">Submit</button>
            <button type="reset" class="btn btn-info">Reset</button>

        </form>

    </div>


    <div class="col-md-2" > </div>


</div>

<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

<script>


    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    });




</script>


</body>
</html>