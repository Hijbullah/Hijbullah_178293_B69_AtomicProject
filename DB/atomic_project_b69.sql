-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2017 at 12:32 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b69`
--
CREATE DATABASE IF NOT EXISTS `atomic_project_b69` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `atomic_project_b69`;

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

DROP TABLE IF EXISTS `birthday`;
CREATE TABLE `birthday` (
  `id` int(13) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`, `is_trashed`) VALUES
(3, 'Hijbullah', '2017-10-01', '2017-10-02 16:23:10'),
(4, 'Jannat', '2017-10-01', '2017-10-02 16:23:16'),
(6, 'Piaro', '2017-10-17', 'NO'),
(7, 'Shofiqul Islam', '2017-10-01', 'NO'),
(8, 'BITM', '2017-10-17', 'NO'),
(9, 'BITM', '2017-10-12', 'NO'),
(10, '1212l;jlhlk', '2017-10-20', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

DROP TABLE IF EXISTS `book_title`;
CREATE TABLE `book_title` (
  `id` int(13) NOT NULL,
  `book_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_trashed`) VALUES
(1, 'Amar Boi', 'Hijbullah Amin', 'NO'),
(2, 'Book name 2', 'author name 2', 'NO'),
(3, 'book name 3', 'author name 3', 'NO'),
(4, 'book name 4', 'author name 4', '2017-10-02 14:24:40'),
(5, 'book name 5', 'book name 5', '2017-10-02 14:24:40'),
(6, 'book name 6', 'author name 6', '2017-10-02 14:24:40');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(13) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `is_trashed`) VALUES
(4, 'Nusrat', 'Chittagong', 'NO'),
(5, 'Shofiqul Islam', 'Barishal', 'NO'),
(6, 'Piaro', 'Sylhet', 'NO'),
(7, 'Hijbullah', 'Rajshahi', 'NO'),
(8, 'Hijbullah', 'Comilla', 'NO'),
(9, 'Shofiqul Islam', 'Dhaka', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
CREATE TABLE `email` (
  `id` int(13) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `is_trashed`) VALUES
(9, 'Shofiqul Islam', 'piaro22440@gmail.com', '2017-10-02 04:26:49'),
(10, 'Shofiqul Islam', 'hijbullaah@gmail.com', '2017-10-02 04:26:49'),
(11, 'Jannat', 'hijbullaah@gmail.com', '2017-10-02 04:26:49'),
(12, 'Hijbullah', 'prof.shafique@gmail.com', '2017-10-02 04:26:49'),
(13, 'Shofiqul Islam', 'hijbullaah@gmail.com', '2017-10-02 04:26:49'),
(14, 'Shofiqul Islam', 'hijbullaah@gmail.com', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
CREATE TABLE `gender` (
  `id` int(13) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `is_trashed`) VALUES
(3, 'Hijbullah', 'Male', 'NO'),
(4, 'Piaro', 'Male', 'NO'),
(5, 'Nusrat', 'Female', 'NO'),
(6, 'Jannat', 'Female', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

DROP TABLE IF EXISTS `hobbies`;
CREATE TABLE `hobbies` (
  `id` int(13) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hobbies` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `is_trashed`) VALUES
(1, 'Shofiqul Islam', 'Eating, Riding, Photography', '2017-10-02 14:17:30'),
(2, 'Nusrat', 'Eating, Riding', '2017-10-02 14:17:30'),
(3, 'Jannat', 'Eating, Riding', '2017-10-02 14:17:30'),
(4, 'Nusrat', 'Riding, Photography', '2017-10-02 14:17:30'),
(5, 'Shafique', 'Eating, Riding, Photography', '2017-10-02 14:17:32'),
(6, 'Piaro', 'Riding, Photography', '2017-10-02 14:17:32'),
(7, 'Kuddus', 'Eating, Riding', '2017-10-02 14:17:32');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

DROP TABLE IF EXISTS `profile_picture`;
CREATE TABLE `profile_picture` (
  `id` int(13) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_picture`, `is_trashed`) VALUES
(1, 'hijbullah', '150693111221191456_750172571845326_1261291997_o.jpg', 'NO'),
(2, 'okhoa', '15069311231486835866atomic11.png', 'NO'),
(3, '1212l;jlhlk', '15069317251486835866atomic11.png', 'NO'),
(4, 'hij', '150693205521191456_750172571845326_1261291997_o.jpg', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

DROP TABLE IF EXISTS `summary_of_organization`;
CREATE TABLE `summary_of_organization` (
  `id` int(13) NOT NULL,
  `organization_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `organization_name`, `summary`, `is_trashed`) VALUES
(4, 'Yahoo', 'readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'l', 'NO'),
(5, 'FaceBook', '<p>readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'l</p>', 'NO'),
(6, 'Google', 'readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'l', 'NO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
